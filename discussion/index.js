// express dependencies

const express = require("express");
const app = express();
const port = 4000;
const mongoose = require("mongoose")

// Middlewares

mongoose.connect("mongodb+srv://mgcuaresma:admin@wdc028-course-booking.cgrme.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology :true
})
// const db = require("db")
// db.once("open", () => console.log("Successfully connected to the database"))


app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Mongoose Schemas
	// schema == blueprint of your data

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

//Model
const Task = mongoose.model("Task", taskSchema);

// Business Logic

// Creating a new task
app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		if (result !== null && result.name === req.body.name) {
			return res.send(`Duplicate task found: ${err}`);
		} else {
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr, savedTask) => {
				if (saveErr) {
					return console.error(saveErr);
				} else {
					return res.status(200).send(`New task created : ${savedTask}`);
				};
			});
		};
	});
});

// retrieving all tasks
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if (err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				tasks: result
			});
		};
	});
});

app.put("/tasks/update/:taskId", (req, res) => {
	let taskId = req.params.taskId;
	let name = req.body.name;

	Task.findByIdAndUpdate(taskId, {name: name}, (err, updatedTask) => {
		if(err) {
			console.log(err);
		} else {
			res.send(`Congratulations, the task has been updated`);
		};
	});
});

// Delete task
app.delete("/tasks/archive-tasks/:taskId", (req, res) => {
	let taskId = req.params.taskId;

	Task.findByIdAndDelete(taskId, (err, deletedTask) =>{
		if(err) {
			console.log(err)
		} else {
			res.send(`${deletedTask} has been deleted`)
		}
	})
})

app.listen(port, () => {
	console.log(`Server running at port${port}`);
});
