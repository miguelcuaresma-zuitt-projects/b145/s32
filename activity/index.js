// express dependencies

const express = require("express");
const app = express();
const port = 4000;
const mongoose = require("mongoose");

// middlewares

mongoose.connect("mongodb+srv://mgcuaresma:admin@wdc028-course-booking.cgrme.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology :true
});

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//1

const userSchema = new mongoose.Schema({
	email: String,
	username: String,
	password: String,
	age: Number,
	isAdmin: Boolean, default: false
});

//2. 
const User = mongoose.model("User", userSchema);

//3.
app.post("/users/signup", (req, res) => {
	User.findOne({email: req.body.email}, (err, result) => {
		if (result !== null && result.email === req.body.email) {
			return res.send(`Sorry, ${req.body.email} is already taken.`)
		} else {
			let newUser = new User({
				email: req.body.email,
				username: req.body.username,
				password: req.body.password,
				age: req.body.age
			});
			newUser.save((saveErr, savedUser) => {
				if (saveErr) {
					return console.error(saveErr);
				} else {
					return res.status(200).send(`Hi, ${newUser.username} you have successfully registered.`);
				};
			});
		};
	});
});

//6
app.get("/users", (req, res) => {
	User.find({}, (err, result) => {
		if (err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				Users: result
			});
		};
	});
});

// 8

app.put("/users/update-user/:userId", (req, res) => {
	let userId = req.params.userId;
	let username = req.body.username;

	User.findByIdAndUpdate(userId, {username : username}, (err, updatedUser) => {
		res.send(`You have successfully updated your username`);
	});
});

app.delete("/users/archive-user/:userId", (req, res) => {
	let userId = req.params.userId;
	User.findByIdAndDelete(userId, (err, deletedUser) => {
		res.send(`${deletedUser} has been deleted.`)
	})
})

app.listen(port, () => {
	console.log(`Server running at port${port}`);
});

